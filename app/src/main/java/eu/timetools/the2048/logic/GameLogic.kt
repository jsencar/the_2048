package eu.timetools.the2048.logic

import androidx.lifecycle.LiveData

interface GameLogic {
    data class Tile(val row: Int, val column: Int, val number: Int)
    enum class GameState { READY, ONGOING, WIN, LOSE }

    val boardState: LiveData<List<Tile>>
    val gameState: LiveData<GameState>
    val score: LiveData<Int>

    fun reset()
    fun swipeUp()
    fun swipeDown()
    fun swipeLeft()
    fun swipeRight()
}