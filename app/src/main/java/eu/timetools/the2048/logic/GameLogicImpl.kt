package eu.timetools.the2048.logic

import android.util.Log
import androidx.lifecycle.MutableLiveData

class GameLogicImpl : GameLogic {
    override val boardState = MutableLiveData(listOf<GameLogic.Tile>())
    override val gameState = MutableLiveData(GameLogic.GameState.READY)
    override val score = MutableLiveData(0)

    private val mBoardState = Array(4) { Array<Int?>(4) { null } }
    private var mScore = 0
        set(value) {
            field = value
            score.postValue(value)
        }

    override fun reset() {
        mScore = 0
        mBoardState.forEach { it.fill(null) }
        addNewTile()
        addNewTile()
        reportNewState()
    }

    override fun swipeUp() {
        for (iColumn in mBoardState[0].indices) {
            val newColumn = collapseRowToStart(mBoardState.map { it[iColumn] }.toTypedArray())
            newColumn.forEachIndexed { iRow, n -> mBoardState[iRow][iColumn] = n }
        }
        addNewTile()
        reportNewState()
    }

    override fun swipeDown() {
        for (iColumn in mBoardState[0].indices) {
            val newColumn =
                collapseRowToStart(mBoardState.map { it[iColumn] }.reversed().toTypedArray())
            newColumn.reversed().forEachIndexed { iRow, n -> mBoardState[iRow][iColumn] = n }
        }
        addNewTile()
        reportNewState()
    }

    override fun swipeLeft() {
        mBoardState.forEachIndexed { iRow, row ->
            mBoardState[iRow] = collapseRowToStart(row)
        }
        addNewTile()
        reportNewState()
    }

    override fun swipeRight() {
        mBoardState.forEachIndexed { iRow, row ->
            mBoardState[iRow] = collapseRowToStart(row.reversedArray()).reversedArray()
        }
        addNewTile()
        reportNewState()
    }

    private fun collapseRowToStart(tiles: Array<Int?>): Array<Int?> {
        val outArray = Array<Int?>(4) { null }

        var slideNSpaces = 0
        var lastIsAlreadyMerged = false

        // E.g. [8 2 2 16] -> [8 0 4 16] -> [0 8 4 16]
        tiles.forEachIndexed { i, n ->
            // Slide
            outArray[i - slideNSpaces] = n
            // Merge
            if (n != null) {
                if (!lastIsAlreadyMerged && outArray.getOrNull(i - slideNSpaces - 1) == n) {
                    outArray[i - slideNSpaces - 1] = n * 2
                    outArray[i - slideNSpaces] = null
                    mScore += n * 2
                    slideNSpaces++
                    lastIsAlreadyMerged = true
                } else {
                    lastIsAlreadyMerged = false
                }
            } else {
                slideNSpaces++
            }
        }
        return outArray
    }


    private fun addNewTile() {
        val emptyPlaces = getEmptyPlaces()

        if (emptyPlaces.isEmpty()) return

        val newTileValue = if ((0..9).random() == 0) 4 else 2
        val newTilePosition = emptyPlaces.random()

        mBoardState[newTilePosition.first][newTilePosition.second] = newTileValue
    }

    private fun reportNewState() {
        val nonEmptyTiles = mBoardState.mapIndexed { iRow, row ->
            row.mapIndexed { iColumn, n ->
                n?.let { GameLogic.Tile(row = iRow, column = iColumn, number = n) }
            }
        }.flatten().filterNotNull()
        boardState.postValue(nonEmptyTiles)

        if (winCondition()) {
            if (gameState.value != GameLogic.GameState.WIN)
                gameState.postValue(GameLogic.GameState.WIN)
        } else if (loseCondition()) {
            gameState.postValue(GameLogic.GameState.LOSE)
        }
        Log.d("TileState", mBoardState.joinToString("\n") { it.joinToString(" ") })
    }

    private fun getEmptyPlaces() = mBoardState.mapIndexed { iRow, row ->
        row.mapIndexed { iColumn, n ->
            if (n == null) Pair(iRow, iColumn) else null
        }
    }.flatten().filterNotNull()

    private fun winCondition(): Boolean {
        val maxValue = mBoardState.maxOf { row -> row.maxOf { it ?: 0 } }
        return maxValue >= 2048
    }

    private fun loseCondition(): Boolean {
        // check empty spaces
        if (getEmptyPlaces().isNotEmpty())
            return false
        // check rows
        mBoardState.forEach { row ->
            var p: Int? = null
            row.forEach {
                if (it == p)
                    return false
                else
                    p = it
            }
        }
        // check columns
        mBoardState[0].indices.forEach { iColumn ->
            var p: Int? = null
            mBoardState.indices.forEach { iRow ->
                if (mBoardState[iRow][iColumn] == p)
                    return false
                else
                    p = mBoardState[iRow][iColumn]
            }
        }
        return true
    }

    init {
        reset()
    }
}