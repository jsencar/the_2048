package eu.timetools.the2048.ui

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import eu.timetools.the2048.R

class TileView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    var number: Int? = null
        set(value) {
            if (field != value) {
                field = value
                applyNumber(value)
            }
        }

    private fun applyNumber(n: Int?) {
        isVisible = n != null
        if (n == null)
            return
        setBackgroundColor(
            ContextCompat.getColor(
                context,
                when (n) {
                    2 -> R.color.tile_2
                    4 -> R.color.tile_4
                    8 -> R.color.tile_8
                    16 -> R.color.tile_16
                    32 -> R.color.tile_32
                    64 -> R.color.tile_64
                    128 -> R.color.tile_128
                    256 -> R.color.tile_256
                    512 -> R.color.tile_512
                    1024 -> R.color.tile_1024
                    2048 -> R.color.tile_2048
                    else -> R.color.tile_over_2048
                }
            )
        )
        setTextColor(
            ContextCompat.getColor(
                context,
                when (n) {
                    2, 4 -> R.color.tile_text_dark
                    else -> R.color.tile_text_light
                }
            )
        )
        setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(
                if (n < 1000)
                    R.dimen.tile_font_size_large
                else
                    R.dimen.tile_font_size_small
            )
        )
        text = n.toString()
    }
}