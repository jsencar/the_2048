package eu.timetools.the2048.ui

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import eu.timetools.the2048.R
import eu.timetools.the2048.logic.GameLogic
import eu.timetools.the2048.logic.GameLogicImpl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val gameLogic: GameLogic by lazy { GameLogicImpl() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gameLogic.boardState.observe(this) {
            refreshGrid(it)
        }

        gameLogic.gameState.observe(this) {
            when (it) {
                GameLogic.GameState.WIN -> showWinDialog()
                GameLogic.GameState.LOSE -> showLoseDialog()
                else -> Unit
            }
        }

        gameLogic.score.observe(this) {
            score.text = it.toString()
        }

        grid.setOnTouchListener(object : OnSwipeTouchListener(this@MainActivity) {
            override fun onSwipeRight() {
                gameLogic.swipeRight()
                Log.d("TileSwipe", "swipeRight")
            }

            override fun onSwipeLeft() {
                gameLogic.swipeLeft()
                Log.d("TileSwipe", "swipeLeft")
            }

            override fun onSwipeUp() {
                gameLogic.swipeUp()
                Log.d("TileSwipe", "swipeUp")
            }

            override fun onSwipeDown() {
                gameLogic.swipeDown()
                Log.d("TileSwipe", "swipeDown")
            }
        })

        reset_button.setOnClickListener {
            if (gameLogic.gameState.value != GameLogic.GameState.READY)
                showResetDialog()
            else
                gameLogic.reset()
        }
    }

    private fun refreshGrid(tiles: List<GameLogic.Tile>) {
        val numberGrid = Array(4) { Array<Int?>(4) { null } }
        tiles.forEach { tile -> numberGrid[tile.row][tile.column] = tile.number }
        tile_c0_r0.number = numberGrid[0][0]
        tile_c0_r1.number = numberGrid[0][1]
        tile_c0_r2.number = numberGrid[0][2]
        tile_c0_r3.number = numberGrid[0][3]
        tile_c1_r0.number = numberGrid[1][0]
        tile_c1_r1.number = numberGrid[1][1]
        tile_c1_r2.number = numberGrid[1][2]
        tile_c1_r3.number = numberGrid[1][3]
        tile_c2_r0.number = numberGrid[2][0]
        tile_c2_r1.number = numberGrid[2][1]
        tile_c2_r2.number = numberGrid[2][2]
        tile_c2_r3.number = numberGrid[2][3]
        tile_c3_r0.number = numberGrid[3][0]
        tile_c3_r1.number = numberGrid[3][1]
        tile_c3_r2.number = numberGrid[3][2]
        tile_c3_r3.number = numberGrid[3][3]
    }

    private fun showResetDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.reset)
            .setMessage(R.string.reset_msg)
            .setPositiveButton(R.string.ok) { _, _ -> gameLogic.reset() }
            .setNeutralButton(R.string.cancel, null)
            .show()
    }

    private fun showWinDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.win)
            .setMessage(R.string.win_msg)
            .setPositiveButton(R.string.keep_playing, null)
            .setNegativeButton(R.string.reset) { _, _ -> gameLogic.reset() }
            .show()
    }

    private fun showLoseDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.lose)
            .setMessage(R.string.lose_msg)
            .setPositiveButton(R.string.start_again) { _, _ -> gameLogic.reset() }
            .setNeutralButton(R.string.close, null)
            .show()
    }

    /**
     * Allows the use of keyboard on emulator
     */

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_DPAD_UP -> gameLogic.swipeUp()
            KeyEvent.KEYCODE_DPAD_DOWN -> gameLogic.swipeDown()
            KeyEvent.KEYCODE_DPAD_LEFT -> gameLogic.swipeLeft()
            KeyEvent.KEYCODE_DPAD_RIGHT -> gameLogic.swipeRight()
            else -> return super.onKeyDown(keyCode, event)
        }
        return true
    }
}